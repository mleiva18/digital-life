<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/new-login',function(){
    return view ('newLogin');
});

Route::get('/', function () {
    return view('auth.login');
});

/*Route::get('/home', function () {
    return view('layouts.dashboard');
})->name('home'); */

Route::group(['middleware' => 'auth'], function(){ 

    Route::get('/home', 'ClientesController@dash')->name('home');

    /*Route::get('/clientes', 'ClientesController@index');

    Route::get('/clientes/create', 'ClientesController@create');*/

    Route::resource ('/clientes','ClientesController');

    Route::group(['middleware' => ['web']],function(){
        Route::resource('clientes','ClientesController');
    });

    Route::get('/clientes', 'ClientesController@index')->name('clientes');

    Route::resource ('/trabajos','TrabajosController');

    Route::group(['middleware' => ['web']],function(){
        Route::resource('trabajos','TrabajosController');
    });

    Route::get('/trabajos', 'TrabajosController@index')->name('trabajos');

    Route::get('/clientes/{id}/view','ClientesController@view');

    Route::resource ('/items','ItemController');

    Route::group(['middleware' => ['web']],function(){
        Route::resource('items','ItemController');
    });

    Route::get('/trabajos/{id}/view','TrabajosController@view');

    Route::get('/facturas', 'FacturaController@index');

    Route::resource ('/facturas','FacturaController');

    Route::get('/entrega','FacturaController@entrega');

    Route::get('/trabajos/{id}/entrega','FacturaController@factura');

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

    

});
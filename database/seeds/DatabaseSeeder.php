<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(EstadosSeeder::class);
        $this->call(MarcasSeeder::class);
        $this->call(ClientesSeeder::class);
        $this->call(UsersSeeder::class);
    }
}

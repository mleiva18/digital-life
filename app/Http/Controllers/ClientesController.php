<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Builde;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Trabajo;


class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $pepe = $request->get('nombre');
        $toto = $request->get('email');
        $clientes = Cliente::orderBy('nombre','ASC')
            ->name($pepe)
            ->email($toto)
            ->paginate(3);

        return view ('clientes.index',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes=Cliente::all();
        return view ('clientes.create',compact("clientes"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clientes=$request->all();

        Cliente::create($clientes);

        return redirect('/clientes')->with('success','Cliente agregado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes=Cliente::findOrFail($id);
        //dd($clientes);
        return view('clientes.show',compact('clientes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientes=Cliente::findOrFail($id);
        return view('clientes.edit',compact('clientes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clientes=Cliente::findOrFail($id);

        $clientes->update($request->all());
        //dd($clientes);
        return redirect('/clientes')->with('success','Cliente Modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view($id)
    {
        $clientes = DB::table('clientes')
        ->where('id','=',$id)
        ->first();

        $trabajos= DB::table('trabajos')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->join('marcas','trabajos.marca_id','=','marcas.id')
        ->select('clientes.*','trabajos.*','clientes.id as clien_id','clientes.comentario as comen','marcas.*','trabajos.id as trab_id')
        ->where('trabajos.cliente_id','=',$id)
        ->orderBy('trabajos.id','DESC')
        ->get();

        //dd($trabajos);
        return view('clientes.view',compact('clientes','trabajos'));
    }

    public function dash()
    {
        /*$pendiente = Trabajo::join('clientes','trabajos.cliente_id','=','clientes.id')
        ->where('estado_id','=','1')
        ->orderBy('trabajos.id','DESC')
        ->limit(5)
        ->get();*/
        $cpendiente = Trabajo::where('estado_id','=','1')->count();
        $ctrabajando = Trabajo::where('estado_id','=','2')->count();
        $cesperando = Trabajo::where('estado_id','=','3')->count();
        $cfalta = Trabajo::where('estado_id','=','4')->count();
        $cterminado = Trabajo::where('estado_id','=','6')->count();
        $centregado = Trabajo::where('estado_id','=','5')->count();

        

        //dd($recibido);
        return view ('layouts.dashboard',compact('cpendiente','ctrabajando','cesperando','cfalta','cterminado','centregado'));
    }
}

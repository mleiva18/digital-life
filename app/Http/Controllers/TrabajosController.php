<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Trabajo;
use App\Models\Cliente;
use App\Models\Marca;
use App\Models\Estado;
use App\Models\Item;

class TrabajosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $norden = $request->get('orden');
        $napellido = $request->get('apellido');
        
        $pedidos=Trabajo::join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->select('trabajos.*','marcas.marca','clientes.*','trabajos.id as orden')
        ->orden($norden)
        ->cliente($napellido)
        ->orderBy('trabajos.id','DESC')
        ->get();
        
//dd($pedidos);
        return view ('trabajos.index',compact('pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trabajos=Trabajo::all();
        $clientes=Cliente::all();
        $marcas=Marca::all();
        $estados=Estado::all();
        return view ('trabajos.create',compact("trabajos",'clientes','marcas','estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trabajos=$request->all();
        //dd($trabajos);
        Trabajo::create($trabajos);

        return redirect('/trabajos')->with('success','Trabajo agregado.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes= DB::table('trabajos')
        ->join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->join('estados','trabajos.estado_id','=','estados.id')
        ->select('trabajos.*','marcas.marca','clientes.*','trabajos.id as orden','estados.nombre as est','trabajos.comentario as comen')
        ->where('trabajos.id','=',$id)
        ->first();
        

//dd($clientes);
        $pdf = PDF::loadView('trabajos.show',compact('clientes'));

        return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trabajos= DB::table('trabajos')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('estados','trabajos.estado_id','=','estados.id')
        ->select('clientes.*','trabajos.*','clientes.id as clien_id','clientes.comentario as comen','marcas.*','trabajos.id as trab_id', 'estados.nombre as est_nom')
        ->where('trabajos.id','=',$id)
        ->first();

        /*$estados = Estado::all();*/
        $estados = Estado::pluck('nombre', 'id');

        $existe = DB::table('items')
        ->where('trabajo_id','=',$id)
        ->first();
        $items = Item::where('trabajo_id','=',$id)->get();
//dd($existe);
        return view('trabajos.edit',compact('trabajos','estados','items','existe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function view(Request $request,$id)
    {
        $norden = $request->get('orden');
        $napellido = $request->get('apellido');

        $pedidos = Trabajo::join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->select('trabajos.*','marcas.marca','clientes.*','trabajos.id as orden')
        ->orden($norden)
        ->cliente($napellido)
        ->where('trabajos.estado_id','=',$id)
        ->orderBy('trabajos.id','DESC')
        ->get();

        return view ('trabajos.view',compact('pedidos'));
    }
}

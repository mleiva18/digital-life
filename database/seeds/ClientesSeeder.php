<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nombre' => 'Martin',
            'apellido' => 'Leiva',
            'documento' => '31561500',
            'password' => '123456789',
            'telefono' => '2914263403',
            'email' => 'ml.serviciotecnico@gmail.com',
            'direccion_calle' => 'Piccioli',
            'direccion_numero' => '3170',
            'direccion_piso' => '',
            'direccion_dpto' => '',
            'direccion_ciudad' => 'Bahía Blanca',
            'telefono_alternativo' => '',
               
        ]);
        
    }
}

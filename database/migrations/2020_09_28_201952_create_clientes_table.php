<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100);
            $table->string('apellido',100);
            $table->string('documento',20);
            $table->string('password',250)->nullable();
            $table->string('telefono',50);
            $table->string('email',50)->nullable();
            $table->string('direccion_calle',150);
            $table->string('direccion_numero',10);
            $table->string('direccion_piso',10)->nullable();
            $table->string('direccion_dpto',10)->nullable();
            $table->string('direccion_ciudad',50);
            $table->string('telefono_alternativo',50);
            $table->string('comentario',200)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}

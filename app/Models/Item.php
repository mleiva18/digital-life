<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['trabajo_id','detalle'];

    public function trabajo(){
        return $this->belongsTo('App\Models\Trabajo','trabajo_id','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trabajo extends Model
{
    protected $fillable = ['cliente_id','marca_id','hora','modelo','nro_serie','fecha_recibido','fecha_prometida','comentario','cable_usb','alimentacion','estado_id'];
    
    public function cliente(){
        return $this->belongsTo('App\Models\Cliente','cliente_id','id');
    }

    public function marca(){
        return $this->belongsTo('App\Models\Marca','marca_id','id');
    }

    public function estado(){
        return $this->belongsTo('App\Models\Estado','estado_id','id');
    }

    //Query Scope
    public function scopeOrden($query, $orden)
    {
        if($orden)
            return $query->where('trabajos.id','=',$orden);  
    }

    public function scopeCliente ($query, $apellido)
    {
        if($apellido)
            return $query->where('clientes.apellido','LIKE',"%$apellido%"); 
    }

    public function scopeOrdenestado($query, $orden, $estado)
    {
        if($orden)
            return $query->where('trabajos.id','=',$orden)->where('trabajos.estado_id','=',$estado);  
    }

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marcas')
        ->insert([
                'marca' => 'Epson',
        ]);
        DB::table('marcas')
        ->insert([
                'marca' => 'HP',
        ]);
        DB::table('marcas')
        ->insert([
                'marca' => 'Brother',
        ]);
        DB::table('marcas')
        ->insert([
                'marca' => 'Canon',
        ]);
    }
}

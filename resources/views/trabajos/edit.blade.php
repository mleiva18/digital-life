@extends('../layouts.starter')

@section('contenido')




<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Estados de Pedidos</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/trabajos')}}">Pedidos</a></li>
          <li class="breadcrumb-item active">Estados</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            
            
            <div class="card">
              
              <div class="card-body table-responsive p-0">
                  <table class="table table-hover text-nowrap">
                    <thead>
                      <tr>
                        
                        <th>ID</th>
                        <th>Cliente</th>
                        <th>Fecha Recibida</th>
                        <th>Estado Actual</th>
                        <th>Fecha Prometida</th>
                        <th>Motivo</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{$trabajos->trab_id}}</td>
                        <td>{{$trabajos->apellido}}</td>
                        <td>{{$trabajos->fecha_recibido}}</td>
                        <td>
                        <?php if ($trabajos->est_nom == 'PENDIENTE') { ?><span class="badge bg-danger recibido">PENDIENTE</span> <?php } ?>
                        <?php if ($trabajos->est_nom == 'TRABAJANDO') { ?><span class="badge bg-primary trabajando">TRABAJANDO</span> <?php } ?>
                        <?php if ($trabajos->est_nom == 'ESPERANDO RESPUESTA') { ?><span class="badge bg-warning respuesta">ESPERANDO RESPUESTA</span> <?php } ?>
                        <?php if ($trabajos->est_nom == 'FALTA REPUESTO') { ?><span class="badge bg-info repuesto">FALTA REPUESTO</span> <?php } ?>
                        <?php if ($trabajos->est_nom == 'ENTREGADO') { ?><span class="badge bg-success entregado">ENTREGADO</span> <?php } ?>
                        <?php if ($trabajos->est_nom == 'TERMINADO') { ?><span class="badge bg terminado">TERMINADO</span> <?php } ?>
                        </td>
                        <td>{{$trabajos->fecha_prometida}}</td>
                        <td>{{$trabajos->comentario}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="accordion" id="accordionExample">
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:white;">
                      Ver tareas realizadas
                    </button>
                  </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                  <div class="card-body">  
                    
                    {{($existe == null) ? 'No hay tareas realizadas' : '' }}
                    @foreach($items as $item)
                    <div class="row">
                      
                      <div class="col-sm-2">
                      
                      <?php $new=date("d-m-Y", strtotime($item->created_at)); echo $new; ?>
                      </div>
                      <div class="col-sm-6">
                      {{$item->detalle}}
                      
                      </div>
                    </div>   
                  @endforeach 
                
                
                  </div>
                </div>
              </div>
            </div>

            <!-- Button trigger modal -->
            <?php if($trabajos->estado_id!='5') { ?>
            <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Agregar trabajo
            </button> -->
            
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#estado">
              Agregar Trabajo
            </button>

          
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#entrega">
              Finalizar
            </button> <?php }?>
          </div>

          <!-- Modal CAMBIA ESTADO -->
          <div class="modal fade" id="estado" tabindex="-1" role="dialog" aria-labelledby="estado" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Agregar Trabajo</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                {!! Form::open(['url' => "items", 'method' => 'POST']) !!}
                  {{csrf_field()}}
                <div class="modal-body">
                  <input type="hidden" name="trabajo_id" value="{{$trabajos->trab_id}}">
                  
                  {!! Form::label('Estado','',['class'=>'form-control-label']) !!}
                  <!--{!! Form::textarea('detalle','',['class' => 'estado', 'id' =>'nombre']) !!}-->
                  {!! Form::select('estado_id', $estados, $trabajos->estado_id, ['class' => 'form-control']) !!}

                  {!! Form::label('Detalle','',['class'=>'form-control-label']) !!}
                  {!! Form::textarea('detalle','',['class' => 'form-control','nullable'=>'nullable']) !!}
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>


          <!-- Modal AGREGA TRABAJO -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Agregar Trabajo</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                {!! Form::open(['url' => "items", 'method' => 'POST']) !!}
                  {{csrf_field()}}
                <div class="modal-body">
                  <input type="hidden" name="trabajo_id" value="{{$trabajos->trab_id}}">
                  
                  {!! Form::label('Detalle','',['class'=>'form-control-label']) !!}
                  {!! Form::textarea('detalle','',['class' => 'form-control']) !!}
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>

          <!-- Modal ENTREGA -->
          <div class="modal fade" id="entrega" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">ENTREGA</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                {!! Form::open(['url' => "facturas", 'method' => 'POST']) !!}
                  {{csrf_field()}}
                <div class="modal-body">
                  <input type="hidden" name="trabajo_id" value="{{$trabajos->trab_id}}">
                  <input type="hidden" name="cliente_id" value="{{$trabajos->cliente_id}}">
                  
                  {!! Form::label('Detalle','',['class'=>'form-control-label']) !!}
                  {!! Form::textarea('detalle','',['class' => 'form-control']) !!}

                  {!! Form::label('Precio','',['class'=>'form-control-label']) !!}
                  <!--{!! Form::number('monto','',['class' => 'form-control']) !!}-->
                  <input type="float" name="monto" class="form-control">
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
                

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
</section>


@endsection
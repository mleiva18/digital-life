@extends('../layouts.starter')

@section('contenido')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Clientes</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/clientes')}}">Clientes</a></li>
          <li class="breadcrumb-item active">Ver</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          
          <div class="card card-primary">
            <div class="card-header">
              <p class="card-title">Ver Cliente</p>
            </div>
            <!-- /.card-header -->
                <!-- form start -->
         
                  <div class="card-body">
                  {!! Form::open(['url' => "clientes", 'method' => 'POST']) !!}
              {{csrf_field()}}
              <div class="row">
              <h6 class="heading-small text-muted mb-4">Información Usuario</h6>
              
              </div>
                
                
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="nombre">Nombre</label>
                        <input type="text" id="nombre" class="form-control" value="{{$clientes->nombre}}" readonly>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-apellido">Apellido</label>
                        <input type="text" id="input-apellido" class="form-control" value="{{$clientes->apellido}}" readonly>
                      </div>
                    </div>
                  <!--</div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="">Contraseña</label>
                        {!! Form::password('password',['class' => 'form-control']) !!}
                      </div>
                    </div>-->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-dni">DNI</label>
                        <input type="text" id="input-username" class="form-control" value="{{$clientes->documento}}" readonly>
                      </div>
                    </div>
                    <!--<div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-fecha">Fecha de Nacimiento</label>
                        <input type="date" id="input-fecha" class="form-control" value="{{$clientes->fecha_nacimiento}}" readonly>
                      </div>
                    </div>-->
                    <div class="col-lg-6">
                      
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Contacto</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_calle">Dirección</label>
                        <input id="direccion_calle" class="form-control" type="text" value="{{$clientes->direccion_calle}}" readonly>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_numero">Número</label>
                        <input id="direccion_numero" class="form-control" type="text" value="{{$clientes->direccion_numero}}" readonly>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_piso">Piso</label>
                        <input id="direccion_piso" class="form-control" type="text" value="{{$clientes->direccion_piso}}" readonly>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Departamento</label>
                        <input id="direccion_dpto" class="form-control" type="text"value="{{$clientes->direccion_dpto}}" readonly>
                      </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_ciudad">Ciudad</label>
                          <input type="text" id="direccion_ciudad" class="form-control" value="{{$clientes->direccion_ciudad}}" readonly>
                        
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Correo Electrónico</label>
                        <input type="email" id="email" class="form-control" value="{{$clientes->email}}" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="input-email">Teléfono</label>
                            <input type="email" id="telefono" class="form-control" value="{{$clientes->telefono}}" readonly>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="input-email">Teléfono Alternativo</label>
                          <input type="email" id="telefono_emergencia" class="form-control" value="{{$clientes->telefono_alternativo}}" readonly>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Información Extra</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Comentario</label>
                      <textarea rows="4" class="form-control" name="comentario" readonly>{{$clientes->comentario}}</textarea>
                  </div>
                </div>
                <div class="col-md-12" >
                  <div class="row">
                    <div class="col-md-10">
                    </div>
                    
                    <div class="col-md-2 ">
                      <!--{!! Form::submit('Guardar',array('class'=>'btn btn-primary')) !!}-->
                      <a href="{{asset('/clientes/'.$clientes->id.'/edit')}}" class="btn btn-warning btn-md float-sm-right"><span class="far fa-edit"></span></a>
                    </div>
                  </div>
                </div>
                
                {!! Form::close() !!}
                  </div>
                  <!-- /.card-body -->

                  
            </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->


@endsection
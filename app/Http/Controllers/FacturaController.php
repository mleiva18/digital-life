<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use App\Models\Factura;
use App\Models\Trabajo;
use App\Models\Item;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $factura = new Factura;
       
        $factura->trabajo_id = $request->input('trabajo_id');
        $factura->cliente_id = $request->input('cliente_id');
        
        $factura->detalle = $request->input('detalle');
        $factura->monto=$request->input('monto');
        
//dd($factura);
        $factura->save();

        $trabajo=Trabajo::findOrFail($request->input('trabajo_id'));
        $trabajo->estado_id="5";

        $trabajo->save();

        return redirect ('/trabajos/'.$factura->trabajo_id.'/edit')->with('success','Item guardado.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function entrega(Request $request)
    {
        $norden = $request->get('orden');
        $napellido = $request->get('apellido');
        
        $pedidos=Trabajo::join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->select('trabajos.*','marcas.marca','clientes.*','trabajos.id as orden')
        ->orden($norden)
        ->cliente($napellido)
        ->where('trabajos.estado_id','=',5)
        ->orderBy('trabajos.id','DESC')
        ->get();
        return view ('trabajos.terminado',compact('pedidos'));
    }

    public function factura($id)
    {
        $clientes= DB::table('trabajos')
        ->join('marcas','trabajos.marca_id','=','marcas.id')
        ->join('clientes','trabajos.cliente_id','=','clientes.id')
        ->join('estados','trabajos.estado_id','=','estados.id')
        ->select('trabajos.*','marcas.marca','clientes.*','trabajos.id as orden','estados.nombre as est','trabajos.comentario as comen')
        ->where('trabajos.id','=',$id)
        ->first();
        
        $items = Item::where('trabajo_id','=',$id)->get();

        $facturas = Factura::where('facturas.trabajo_id','=',$id)->first();
        //dd($facturas);
        $pdf = PDF::loadView('trabajos.entrega',compact('clientes','items','facturas'));

        return $pdf->stream();
    }
}

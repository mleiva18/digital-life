<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Digital Life - Entrega de Trabajo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="{{asset('estilos.css')}}">
  <!--<link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">-->
  
</head>
<body>
    <table  width="100%">
        <tr>
            <td width="25%" align="center"><img src="{{asset('logos/logo.png')}}" alt="" width="70px">
            <br> <span style="font-size:20px;">Digital Life</span>
            <br> Soluciones en Impresión
            </td>
            <td style="text-align:center;">
            Moreno 430 - Bahía Blanca - Buenos Aires<br>
            (8000) - Tel: 0291-4510815
            <!-- info@digital-life.com.ar <br> -->
            <p style="font-size:20px; padding-top: -10px;"> <strong> SERVICE OFICIAL EPSON </strong></p>
            </td>
            <td width="20%" style="text-align:center;">Orden de Reparación:<br> <strong> {{$clientes->orden}} </strong></td>
        </tr>
    </table>
        
    <table width="100%" style="text-align:center;">
    <tr>
        <td colspan="5" style="text-align:center;">
            <p style="font-size:15px;"><strong>DATOS DEL CLIENTE</strong> </p>
        </td>
    </tr>
    <thead>
    <tr>
    <td style="background-color:grey;color:white;">Nombre</td>
    <td style="background-color:grey;color:white;">Apellido</td>
    <td style="background-color:grey;color:white;">Telefono</td>
    <td style="background-color:grey;color:white;">Direccion</td>
    <td style="background-color:grey;color:white;">Correo Electrónico</td>
    </tr>
    </thead>
    <tr>
    <td>{{$clientes->nombre}}</td>
    <td>{{$clientes->apellido}}</td>
    <td>{{$clientes->telefono}}</td>
    <td>{{$clientes->direccion_calle}} {{$clientes->direccion_numero}} <br>{{$clientes->direccion_piso}} {{$clientes->direccion_dpto}}</td>
    <td>{{$clientes->email}} </td>
    </tr>
</table>

<table style="border:solid 1px;" width="100%">
<tr>
    <td width="40%">
        <table class="informe">
            <tr>
                <td>
                    <h3>DATOS DEL EQUIPO</h3>
                </td>
            </tr>
            
            <tr>
                <td><b>Marca:</b> {{$clientes->marca}}</td>
            </tr>
            <tr>
                <td><b>Modelo:</b>{{$clientes->modelo}}</td>
            </tr>
            <tr>
                <td><b>N° de Serie:</b> {{$clientes->nro_serie}}</td>
            </tr>
            <tr>
                <td><b>Cable de Alimentación:</b> {{($clientes->alimentacion == "1") ? 'SI' : 'NO' }}</td>
            </tr>
            <tr>
                <td><b>Cable USB:</b> {{($clientes->cable_usb == "1") ? 'SI' : 'NO' }}</td>
            </tr>
        </table>
    </td>

    <td  width="100%"> 
        <table class="informe" width="100%">
            <tr>
                <td style="text-align:center;">
                    <h3>DETALLE</h3>
                </td>
            </tr>
            <tr>
                <td>Motivo:</td>
            </tr>
            <tr>
                <td style="border: solid 1px;padding:20px; text-align:left;" width="100%" height="80px">{{$clientes->comen}}</td>
            </tr>
        </table>

    </td>

</tr>

</table>
<br>
<table width="100%" style="text-align:center;">
    <tr>
        <td style="background-color:grey;color:white;">Fecha de Ingreso</td>
        <td style="background-color:grey;color:white;">Hora Ingreso</td>
        <td style="background-color:grey;color:white;">Fecha Prometida</td>
        <td style="background-color:grey;color:white;">Estado</td>
        <td style="background-color:grey;color:white;">Técnico</td>
    </tr>
    <tr>
        <td><?php $new=date("d-m-Y", strtotime($clientes->fecha_recibido)); echo $new; ?></td>
        <td>{{$clientes->hora}}</td>
        <td><?php $new=date("d-m-Y", strtotime($clientes->fecha_prometida)); echo $new; ?></td>
        <td>{{$clientes->est}}</td>
        <td>Daniel-Luis</td>
    </tr>
    <tr>
        <td><img src="{{asset('logos/epson.png')}}" alt="" width="100px"></td>
        <td colspan="2"><img src="{{asset('logos/canon.png')}}" alt="" width="150px"></td>
        <td><img src="{{asset('logos/hp.png')}}" alt="" width="50px"></td>
        <td><img src="{{asset('logos/ricoh.png')}}" alt="" width="150px"></td>
    </tr>
</table>
<hr style="border: dotted ;">
<p style="text-align:right; border: solid 1px;">Orden de Reparación:<strong> {{$clientes->orden}} </strong></p>

<p style="text-align:center;"><strong>DATOS DEL CLIENTE</strong></p>


<table width="100%" style="text-align:center;">
    <tr>
        <td style="background-color:grey;color:white;">Nombre</td>
        <td style="background-color:grey;color:white;">Apellido</td>
        <td style="background-color:grey;color:white;">Telefono</td>
        <td style="background-color:grey;color:white;">Direccion</td>
        <td style="background-color:grey;color:white;">Correo Electrónico</td>
    </tr>
    <tr>
    <td>{{$clientes->nombre}}</td>
    <td>{{$clientes->apellido}}</td>
    <td>{{$clientes->telefono}}</td>
    <td>{{$clientes->direccion_calle}} {{$clientes->direccion_numero}} <br>{{$clientes->direccion_piso}} {{$clientes->direccion_dpto}}</td>
    <td>{{$clientes->email}} </td>
    </tr>
</table>
<br>

<p style="text-align:center;"><strong>DATOS DEL EQUIPO</strong></p>
            
            </tr>
<table style="border:solid 1px;" width="100%">
            <tr>
                <td><b>Marca:</b> {{$clientes->marca}}</td>
            
                <td><b>Modelo:</b>{{$clientes->modelo}}</td>
            
                <td><b>N° de Serie:</b> {{$clientes->nro_serie}}</td>
            </tr>
            <tr>
                <td><b>Cable de Alimentación:</b> {{($clientes->alimentacion == "1") ? 'SI' : 'NO' }}</td>
            
                <td><b>Cable USB:</b> {{($clientes->cable_usb == "1") ? 'SI' : 'NO' }}</td>
            </tr>
</table>
<br>
<table class="informe" width="100%">
            <tr>
                <td style="text-align:center;">
                    <strong>DETALLE</strong>
                </td>
            </tr>
            <tr>
                <td style="border: solid 1px;" width="100%" height="80px">{{$clientes->comen}}</td>
            </tr>
</table>

<table width="100%" style="text-align:center;">
    <tr>
        <td style="background-color:grey;color:white;">Fecha de Ingreso</td>
        <td style="background-color:grey;color:white;">Hora Ingreso</td>
        <td style="background-color:grey;color:white;">Fecha Prometida</td>
        <td style="background-color:grey;color:white;">Estado</td>
        <td style="background-color:grey;color:white;">Técnico</td>
    </tr>
    <tr>
        <td><?php $new=date("d-m-Y", strtotime($clientes->fecha_recibido)); echo $new; ?></td>
        <td>{{$clientes->hora}}</td>
        <td><?php $new=date("d-m-Y", strtotime($clientes->fecha_prometida)); echo $new; ?></td>
        <td>{{$clientes->est}}</td>
        <td>Daniel-Luis</td>
    </tr>
</table>
</body>
</html>
@extends('../layouts.starter')

@section('contenido')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Pedido</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/clientes')}}">Pedido</a></li>
          <li class="breadcrumb-item active">Nuevo</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Alta Pedido</h3>
            </div>
            <!-- /.card-header -->
                <!-- form start -->
         
                  <div class="card-body">
                  {!! Form::open(['url' => "trabajos", 'method' => 'POST']) !!}
              {{csrf_field()}}
                <h6 class="heading-small text-muted mb-4">Información Usuario</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="nombre">Apellido y Nombre</label>
                        <select name="cliente_id" id="cliente_id" class="form-control" required>
                          <option value="">Seleccione una opción</option>
                          @foreach ($clientes as $cliente)
                            <option value="{{$cliente->id}}">{{$cliente->apellido}} {{$cliente->nombre}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- DATOS EQUIPO -->
                <h6 class="heading-small text-muted mb-4">Datos Equipo</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="marca">Marca Equipo</label>
                          <select name="marca_id" id="marca_id" class="form-control" required>
                            <option value="">Seleccione </option>
                            @foreach ($marcas as $marca)
                              <option value="{{$marca->id}}">{{$marca->marca}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    
                      <div class="col-lg-4">
                        <div class="form-group">
                        {!! Form::label('modelo','',['class'=>'form-control-label']) !!}
                        {!! Form::text('modelo','',['class'=>'form-control','required'=>'required']) !!}
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                        {!! Form::label('nro_serie','',['class'=>'form-control-label']) !!}
                        {!! Form::text('nro_serie','',['class'=>'form-control','required' => 'required']) !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                        {!! Form::label('Cable USB','',['class'=>'form-control-label']) !!}
                        {!! Form::checkbox('cable_usb', '1', false ,['class'=>'label-group-prepend']) !!}
                        
                        
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                        {!! Form::label('Fuente de Alimentación','',['class'=>'form-control-label']) !!}
                        {!! Form::checkbox('alimentacion', '1',false ,['class'=>'label-group-prepend'])!!}
                        </div>
                      </div>
                    </div>
                  </div>
                <hr class="my-4" />
                <h6 class="heading-small text-muted mb-4">Fecha y Estado de Pedido</h6>
                <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-lg-3">
                        <div class="form-group">
                          {!! Form::label('fecha_recibido','',['class'=>'form-control-label']) !!}
                          {!! Form::date('fecha_recibido',date("Y-m-d"),['class'=>'form-control']) !!}
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          {!! Form::label('hora','',['class'=>'form-control-label']) !!}
                          {!! Form::time('hora',date('H:i'),['class'=>'form-control']) !!}
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          {!! Form::label('fecha_prometida','',['class'=>'form-control-label']) !!}
                          {!! Form::date('fecha_prometida','',['class'=>'form-control']) !!}
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <label class="form-control-label" for="estado_id">Estado Pedido</label>
                          <select name="estado_id" id="estado_id" class="form-control" required>
                            <option value="">Seleccione </option>
                            @foreach ($estados as $estado)
                              <option value="{{$estado->id}}">{{$estado->nombre}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                </div>

                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Información Extra</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Comentario</label>
                    <!--<textarea rows="4" class="form-control" name="comentario"></textarea>-->
                    {!! Form::textarea('comentario','',['class' => 'form-control','required' => 'required']) !!}
                  </div>
                </div>
                <div class="col-md-12" >
                  <div class="row">
                    <div class="col-md-10">
                    </div>
                    
                    <div class="col-md-1">
                      {!! Form::submit('Guardar',array('class'=>'btn btn-primary')) !!}
                      <!--<button type="submit" formtarget="_blank" class="btn btn-primary">TEST</button>-->
                    </div>
                  </div>
                </div>
                
                {!! Form::close() !!}
                  </div>
                  <!-- /.card-body -->

                  
            </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection
@extends('../layouts.starter')

@section('contenido')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Clientes</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/clientes')}}">Clientes</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Editar Cliente</h3>
            </div>
            <!-- /.card-header -->
                <!-- form start -->
         
                  <div class="card-body">
                  {!! Form::open(['url' => "/clientes/$clientes->id", 'method' => 'POST']) !!}
                  {{csrf_field()}}
                  <input type="hidden" name="_method" value="PUT">
                <h6 class="heading-small text-muted mb-4">Información Usuario</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="nombre">Nombre</label>
                        {!! Form::text('nombre',$clientes->nombre,['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="apellido">Apellido</label>
                        {!! Form::text('apellido',$clientes->apellido,['class' => 'form-control']) !!}
                      </div>
                    </div>
                  <!--</div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="">Contraseña</label>
                        {!! Form::password('password',['class' => 'form-control']) !!}
                      </div>
                    </div>-->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="documento">DNI</label>
                        {!! Form::text('documento',$clientes->documento,['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <!--<div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-fecha">Fecha de Nacimiento</label>
                        <input type="date" id="input-fecha" class="form-control" value="{{$clientes->fecha_nacimiento}}" >
                      </div>
                    </div>-->
                    <div class="col-lg-6">
                      
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Contacto</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_calle">Dirección</label>
                        {!! Form::text('direccion_calle',$clientes->direccion_calle,['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_numero">Número</label>
                        {!! Form::text('direccion_numero',$clientes->direccion_numero,['class' => 'form-control']) !!}
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_piso">Piso</label>
                        {!! Form::text('direccion_piso',$clientes->direccion_piso,['class' => 'form-control']) !!}
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_dpto">Departamento</label>
                        {!! Form::text('direccion_dpto',$clientes->direccion_dpto,['class' => 'form-control']) !!}
                      </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="direccion_ciudad">Ciudad</label>
                        {!! Form::text('direccion_ciudad',$clientes->direccion_ciudad,['class' => 'form-control']) !!}
                        
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="email">Correo Electrónico</label>
                        {!! Form::text('email',$clientes->email,['class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="telefono">Teléfono</label>
                          {!! Form::text('telefono',$clientes->telefono,['class' => 'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="telefono_alternativo">Teléfono Alternativo</label>
                          {!! Form::text('telefono_alternativo',$clientes->telefono_alternativo,['class' => 'form-control']) !!}
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Información Extra</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Comentario</label>
                    <!--<textarea rows="4" class="form-control" name="comentario"></textarea>-->
                    {!! Form::textarea('comentario',$clientes->comentario,['class' => 'form-control']) !!}
                  </div>
                </div>
                <div class="col-md-12" >
                  <div class="row">
                    <div class="col-md-10">
                    </div>
                    
                    <div class="col-md-1">
                      {!! Form::submit('Actualizar',array('class'=>'btn btn-primary')) !!}
                    </div>
                  </div>
                </div>
                
                {!! Form::close() !!}
                  </div>
                  <!-- /.card-body -->

                  
            </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection
@extends('../layouts.starter')

@section('contenido')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Clientes</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/clientes')}}">Clientes</a></li>
          <li class="breadcrumb-item active">Nuevo</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
          
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Alta Cliente</h3>
            </div>
            <!-- /.card-header -->
                <!-- form start -->
         
                  <div class="card-body">
                  {!! Form::open(['url' => "clientes", 'method' => 'POST']) !!}
              {{csrf_field()}}
                <h6 class="heading-small text-muted mb-4">Información Usuario</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-nombre">Nombre</label>
                        <!--<input type="text" id="input-nombre" class="form-control" value="">-->
                        {!! Form::text('nombre','', array('class'=>'form-control','required' => 'required')) !!}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-apellido">Apellido</label>
                        <!--<input type="text" id="input-apellido" class="form-control" value="">-->
                        {!! Form::text('apellido','',['class'=>'form-control','required' => 'required']) !!}
                      </div>
                    </div>
                  <!--</div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="">Contraseña</label>
                        {!! Form::password('password',['class' => 'form-control']) !!}
                      </div>
                    </div>-->
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-dni">DNI</label>
                        <!--<input type="text" id="input-username" class="form-control" value="">-->
                        {!! Form::text('documento','',['class'=>'form-control']) !!}
                      </div>
                    </div>
                    <!--<div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-fecha">Fecha de Nacimiento</label>
                        
                        {!! Form::date('fecha_nacimiento','',['class'=>'form-control']) !!}
                      </div>
                    </div>-->
                    <div class="col-lg-6">
                      
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Contacto</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Dirección</label>
                        <!--<input id="direccion_calle" class="form-control" type="text">-->
                        {!! Form::text('direccion_calle','',['class'=>'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Número</label>
                        <!--<input id="direccion_numero" class="form-control" type="text">-->
                        {!! Form::text('direccion_numero','',['class'=>'form-control']) !!}
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Piso</label>
                        <!--<input id="direccion_piso" class="form-control" type="text">-->
                        {!! Form::text('direccion_piso','',['class'=>'form-control']) !!}
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Departamento</label>
                        <!--<input id="direccion_dpto" class="form-control" type="text">-->
                        {!! Form::text('direccion_dpto','',['class'=>'form-control']) !!}
                      </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">Ciudad</label>
                        <!--<input type="text" id="direccion_ciudad" class="form-control">-->
                        {!! Form::text('direccion_ciudad','',['class'=>'form-control']) !!}
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Correo Electrónico</label>
                        <!--<input type="email" id="email" class="form-control">-->
                        {!! Form::email('email', '',['class'=>'form-control']) !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="input-email">Teléfono</label>
                          <!--<input type="email" id="telefono" class="form-control">-->
                          {!! Form::text('telefono','',['class'=>'form-control','required' => 'required']) !!}
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                          <label class="form-control-label" for="input-email">Teléfono Alternativo</label>
                          <!--<input type="email" id="telefono_emergencia" class="form-control">-->
                          {!! Form::text('telefono_alternativo','',['class'=>'form-control']) !!}
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">Información Extra</h6>
                <div class="pl-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Comentario</label>
                    <!--<textarea rows="4" class="form-control" name="comentario"></textarea>-->
                    {!! Form::textarea('comentario','',['class' => 'form-control']) !!}
                  </div>
                </div>
                <div class="col-md-12" >
                  <div class="row">
                    <div class="col-md-10">
                    </div>
                    
                    <div class="col-md-1">
                      {!! Form::submit('Guardar',array('class'=>'btn btn-primary')) !!}
                    </div>
                  </div>
                </div>
                
                {!! Form::close() !!}
                  </div>
                  <!-- /.card-body -->

                  
            </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Trabajo;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        if($request->input('detalle')==null){
            $item = new Item;
            
            $item->trabajo_id = $request->input('trabajo_id');
            
            $item->detalle = "Cambio de estado.";

            $item->save();
        }
        else
        {
            $item = new Item;
            
            $item->trabajo_id = $request->input('trabajo_id');
            
            $item->detalle = $request->input('detalle');

            $item->save();
        }
        $trabajo = Trabajo::findOrFail($request->input('trabajo_id'));
        $trabajo->estado_id = $request->input('estado_id');

        $trabajo->save();

        /*$trabajo = Trabajo::findOrFail($item->trabajo_id);
        
        $trabajo->monto=$request->input('monto');
        $trabajo->estado_id="5";
        $trabajo->save();*/

        return redirect ('/trabajos/'.$item->trabajo_id.'/edit')->with('success','Item guardado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view($id)
    {
        //
    }
}

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Digital Life - Entrega de Trabajo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="{{asset('estilos.css')}}">
  <!--<link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">-->
  
</head>
<body>
    <table width="100%" style="text-align:center;">
        <tr>
            <td colspan="5" style="text-align:center;">
                <p style="font-size:15px;"><strong>DATOS DEL CLIENTE</strong> </p>
            </td>
        </tr>
        <thead>
        <tr>
        <td style="background-color:grey;color:white;">Nombre</td>
        <td style="background-color:grey;color:white;">Apellido</td>
        <td style="background-color:grey;color:white;">Telefono</td>
        <td style="background-color:grey;color:white;">Direccion</td>
        <td style="background-color:grey;color:white;">Correo Electrónico</td>
        </tr>
        </thead>
        <tr>
        <td>{{$clientes->nombre}}</td>
        <td>{{$clientes->apellido}}</td>
        <td>{{$clientes->telefono}}</td>
        <td>{{$clientes->direccion_calle}} {{$clientes->direccion_numero}} <br>{{$clientes->direccion_piso}} {{$clientes->direccion_dpto}}</td>
        <td>{{$clientes->email}} </td>
        </tr>
    </table>

    <table  width="100%" style="text-align:center;">
        <tr>
            <td colspan="3">
            <p style="font-size:15px;"><strong>DATOS DEL EQUIPO</strong> </p>
            </td>
        </tr>
        
        <tr>
            <td style="background-color:grey;color:white;">Marca</td>
            <td style="background-color:grey;color:white;">Modelo</td>
            <td style="background-color:grey;color:white;">N° Serrie</td>
            <td style="background-color:grey;color:white;">Cable Alimentación</td>
            <td style="background-color:grey;color:white;">Cable USB</td>
        </tr>
        <tr>
            <td>{{$clientes->marca}}</td>
            <td>{{$clientes->modelo}}</td>
            <td>{{$clientes->nro_serie}}</td>
            <td>{{($clientes->alimentacion == "1") ? 'SI' : 'NO' }}</td>
            <td>{{($clientes->cable_usb == "1") ? 'SI' : 'NO' }}</td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td style="text-align:center;">
                <h3>DETALLE</h3>
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px;padding:20px; text-align:left;" width="100%" height="80px">{{$clientes->comen}}</td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <td style="text-align:center;">
                <h3>RESOLUCIÓN</h3>
            </td>
        </tr>
        <tr>
            <td style="border: solid 1px;padding:20px; text-align:left;" width="100%" height="80px">
            @foreach($items as $item)
                <?php $new=date("d-m-Y", strtotime($item->created_at)); echo $new; ?>
                
                
                {{$item->detalle}}
                <br>
                
            @endforeach 
            {{$facturas->detalle }}
            </td>
        </tr>
        <tr>
        <td colspan="2" style="text-align:right;"><b>Costo: </b> {{$facturas->monto }}</td>
        </tr>
    </table>
</body>
</html>
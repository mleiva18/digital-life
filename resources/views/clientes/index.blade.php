@extends("../layouts.starter")

@section("contenido")

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Búsqueda de Clientes</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/clientes')}}">Clientes</a></li>
          <li class="breadcrumb-item active">Index</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="page-header">
          <h1>
            
            {!! Form::open(['route' => 'clientes', 'method' => 'GET','class' => 'form-inline pull-right']) !!}
              <div class="form-group">
                {!! Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Nombre']) !!}
              </div>
              <div class="form-group">
                {!! Form::text('email',null,['class' => 'form-control','placeholder' => 'Email']) !!}
              </div>
           
              <div class="form-group">
                <button type="submit" class="btn btn-default">
                  <span class="fas fa-search"></span>
                </button>
                <button type="cancel" class="btn btn-default">
                  <span class="far fa-window-close"></span>
                  </button>
              </div>
            {!! Form::close() !!}
          </h1>
        </div>
      </div>
      <div class="col-md-12">
        <table class="table table-hover table-striped">
          <thead style="background-color:#2d2c77;color:white;">
            <tr>
              <td>Nombre</td>
              <td>Apellido</td>
              <td>DNI</td>
              <td>Correo Electrónico</td>
              <td>Teléfono</td>
              <td width="15%"></td>
            </tr>
          </thead>
          <tbody>
            @foreach ($clientes as $cliente)
              <tr>
                <td>{{$cliente->nombre}}</td>
                <td>{{$cliente->apellido}}</td>
                <td>{{$cliente->documento}}</td>
                <td>{{$cliente->email}}</td>
                <td>{{$cliente->telefono}}</td>
                <td><a href="{{asset('/clientes/'.$cliente->id)}}" class="btn btn-primary btn-sm active"><span class="fas fa-eye"></span></a><a href="{{asset('/clientes/'.$cliente->id.'/edit')}}" class="btn btn-warning btn-sm"><span class="far fa-edit"></span></a><a href="{{asset('/clientes/'.$cliente->id.'/view')}}" class="btn btn-info btn-sm active"><span class="fas fa-plus-square"></span></a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {{$clientes->render()}}
      </div>
    </div>
  </div>
      
</section>
<!-- /.content -->

@endsection
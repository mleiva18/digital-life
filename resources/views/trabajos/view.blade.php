@extends('../layouts.starter')

@section('contenido')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Pedidos por estado</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{asset('/trabajos')}}">Pedidos</a></li>
          <li class="breadcrumb-item active">Index</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="page-header">
          <h1>
            
            {!! Form::open(['route' => 'trabajos', 'method' => 'GET','class' => 'form-inline pull-right']) !!}
              <div class="form-group">
                {!! Form::text('apellido',null,['class' => 'form-control','placeholder' => 'Apellido']) !!}
              </div>
              <div class="form-group">
                {!! Form::text('orden',null,['class' => 'form-control','placeholder' => 'Orden']) !!}
              </div>
           
              <div class="form-group">
                <button type="submit" class="btn btn-default">
                  <span class="fas fa-search"></span>
                </button>
                <button type="cancel" class="btn btn-default">
                  <span class="far fa-window-close"></span>
                  </button>
              </div>
            {!! Form::close() !!}
          </h1>
        </div>
      </div>
      <div class="col-md-12">
        <table class="table table-hover table-striped">
          <thead style="background-color:#2d2c77;color:white;">
            <tr>
                <td>Estado</td>
                <td>N° Orden</td>
                <td>Cliente</td>
                <td>Marca</td>
                <td>Modelo</td>
              
              <td width="10%"></td>
            </tr>
          </thead>
          <tbody>
            @foreach($pedidos as $trabajo)
                <tr>
                    <td>
                        <!--{{($trabajo->estado_id != 1) ? ' }} <span class="badge bg-danger etiqueta">PENDIENTE</span>{{' : 'a'}}-->
                        <?php if ($trabajo->estado_id == 1) { ?><span class="badge bg-danger recibido">PENDIENTE</span> <?php } ?>
                        <?php if ($trabajo->estado_id == 2) { ?><span class="badge bg-primary trabajando">TRABAJANDO</span> <?php } ?>
                        <?php if ($trabajo->estado_id == 3) { ?><span class="badge bg-warning respuesta">ESPERANDO RESPUESTA</span> <?php } ?>
                        <?php if ($trabajo->estado_id == 4) { ?><span class="badge bg-info repuesto">FALTA REPUESTO</span> <?php } ?>
                        <?php if ($trabajo->estado_id == 5) { ?><span class="badge bg-success entregado">ENTREGADO</span> <?php } ?>
                        <?php if ($trabajo->estado_id == 6) { ?><span class="badge bg terminado">TERMINADO</span> <?php } ?>
                    </td>
                    <td>{{$trabajo->orden}}</td>
                    <td>{{$trabajo->apellido}} {{$trabajo->nombre}}</td>
                    <td>{{$trabajo->marca}}</td>
                    <td>{{$trabajo->modelo}}</td>
                    <td><!--{{$trabajo->id}}--> <a href="{{asset('/trabajos/'.$trabajo->orden.'/edit')}}" class="btn btn-warning btn-sm"><span class="far fa-edit"></span></a><!--{{$trabajo->orden}}--> <a href="{{asset('/trabajos/'.$trabajo->orden)}}" class="btn btn-info btn-sm active" target="_blank"><span class="far fa-file-pdf"></span></a></td>
                </tr>
            @endforeach
          </tbody>
        </table>
        
      </div>
    </div>
  </div>
      
</section>
<!-- /.content -->

@endsection
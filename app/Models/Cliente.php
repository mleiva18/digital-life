<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Cliente extends Model
{
    protected $fillable = ['nombre','apellido','documento','password','telefono','email','direccion_calle','direccion_numero','direccion_piso','direccion_dpto','direccion_ciudad','telefono_alternativo','comentario'];

    //Query Scope
    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('nombre','LIKE',"%$name%"); 
    }

    public function scopeEmail($query, $correo)
    {
        if($correo)
            return $query->where('email','LIKE',"%$correo%"); 
    }

}
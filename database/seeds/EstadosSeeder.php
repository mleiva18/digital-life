<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert(['nombre' => 'PENDIENTE',]);
        DB::table('estados')->insert(['nombre' => 'TRABAJANDO',]);
        DB::table('estados')->insert(['nombre' => 'ESPERANDO RESPUESTA',]);
        DB::table('estados')->insert(['nombre' => 'FALTA REPUESTO',]);
        DB::table('estados')->insert(['nombre' => 'ENTREGADO',]);
        DB::table('estados')->insert(['nombre' => 'TERMINADO',]);
    }
}
